import Axios from "axios";
import { DOMAIN, TOKEN, TOKEN_CYBER } from "../util/settings/config";
import { store } from "../redux/configStore";
import {
  displayLoadingAction,
  hideLoadingAction,
} from "../redux/actions/LoadingAction";

export class baseService {
  //put json về phía backend
  put = (url, model) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "PUT",
      data: model,
      headers: {
        Authorization: "Bearer " + JSON.parse(localStorage.getItem(TOKEN)),
        TokenCybersoft: TOKEN_CYBER,
      }, //JWT
    });
  };

  post = (url, model) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "POST",
      data: model,
      headers: {
        Authorization: "Bearer " + JSON.parse(localStorage.getItem(TOKEN)),
        TokenCybersoft: TOKEN_CYBER,
      }, //JWT
    });
  };

  get = (url) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem(TOKEN),
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  };

  delete = (url) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + JSON.parse(localStorage.getItem(TOKEN)),

        TokenCybersoft: TOKEN_CYBER,
      },
    });
  };
}
// Add a request interceptor
Axios.interceptors.request.use(
  function (config) {
    store.dispatch(displayLoadingAction);

    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
Axios.interceptors.response.use(
  function (response) {
    setTimeout(() => {
      store.dispatch(hideLoadingAction);
    }, 1000);
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    setTimeout(() => {
      store.dispatch(hideLoadingAction);
    }, 1000);

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
