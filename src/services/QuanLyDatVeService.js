import { GROUPID } from "../util/settings/config";
import { ThongTinDatVe } from "../_core/models/ThongTinDatVe";
import { baseService } from "./baseService";

export class QuanLyDatVeService extends baseService {
  constructor() {
    super();
  }

  layChiTietPhongVe = (malichChieu) => {
    return this.get(
      `api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${malichChieu}`
    );
  };
  datVe = (thongTinDatVe = new ThongTinDatVe()) => {
    return this.post(`api/QuanLyDatVe/DatVe`, thongTinDatVe);
  };
  taoLichChieu = (data) => {
    return this.post("api/QuanLyDatVe/TaoLichChieu", data);
  };
}

export const quanLyDatVeService = new QuanLyDatVeService();
