import React from "react";
import { useDispatch } from "react-redux";
import Swal from "sweetalert2";
import { LoginOutlined } from "@ant-design/icons";
import { DANG_XUAT } from "../../redux/actions/types/QuanLyNguoiDungType";

export default function Logout() {
  const dispatch = useDispatch();

  const HandleClick = () => {
    Swal.fire({
      title: "Bạn có muốn đăng xuất?",

      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Hủy",
      confirmButtonText: "Đồng ý!",
    }).then((res) => {
      if (res.isConfirmed) {
        dispatch({ type: DANG_XUAT });
        window.location.href = "/";
      }
    });
  };
  return (
    <div className="ml-2 flex items-center ">
      <button
        class="font-medium text-gray-400 text-lg text-center hover:text-red-400"
        onClick={HandleClick}
      >
        Đăng xuất
      </button>
    </div>
  );
}
