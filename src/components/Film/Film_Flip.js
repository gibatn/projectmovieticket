import React from "react";
import { NavLink } from "react-router-dom";
import "./Film_Flip.css";
export default function Film_Flip({ phim }) {
  return (
    <div className="pb-10 flex justify-center items-center w-full">
      <div class="flip-card ">
        <div class="flip-card-inner rounded-xl">
          <div class="flip-card-front rounded-xl">
            <img src={phim.hinhAnh} alt="Avatar" className="rounded-xl" />
          </div>
          {/* <div
            class="flip-card-back"
            style={{ position: "relative", backgroundColor: "rgba(0,0,0,.9" }}
          >
            <div style={{ position: "absolute", top: 0, left: 0 }}>
              <img
                src={phim.hinhAnh}
                alt="hinhanh"
                style={{ width: 300, height: 300 }}
              />
            </div>
            <div
              className="w-full h-full"
              style={{
                position: "absolute",
                backgroundColor: "rgba(0,0,0,.5",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <div>
                <h1 className="text-xl mt-2 font-bold text-orange-500">
                  Phim : {phim.tenPhim}
                </h1>
                <p className=" font-bold text-orange-300">
                  {phim.moTa.length > 100
                    ? phim.moTa.slice(0, 100) + "..."
                    : phim.moTa}
                </p>
              </div>
            </div>
          </div> */}
        </div>
        <NavLink to={`detail/${phim.maPhim}`}>
          <div className="dat-ve  ">ĐẶT VÉ</div>
        </NavLink>
      </div>
    </div>
  );
}
