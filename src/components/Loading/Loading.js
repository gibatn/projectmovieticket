import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import loadinganimation from "./loadingLottie.json";
export default function Loading() {
  let { isLoading } = useSelector((state) => state.LoadingReducer);

  return (
    <>
      {isLoading ? (
        <div
          className="text-2xl"
          style={{
            position: "fixed",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0,0,0,.5",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",

            zIndex: 99,
          }}
        >
          <Lottie animationData={loadinganimation} />
        </div>
      ) : (
        ""
      )}
    </>
  );
}
