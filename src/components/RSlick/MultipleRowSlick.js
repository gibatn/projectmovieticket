import React from "react";
import Slider from "react-slick";
import { useDispatch, useSelector } from "react-redux";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./MultipleRowSlick.css";
import Film_Flip from "../Film/Film_Flip";
import {
  SET_FILM_DANG_CHIEU,
  SET_FILM_SAP_CHIEU,
} from "../../redux/actions/types/QuanLyPhimStyle";

const MultipleRowSlick = ({ arrFilm }) => {
  const { dangChieu, sapChieu } = useSelector(
    (state) => state.QuanLyPhimReducer
  );
  let dispatch = useDispatch();
  let renderFilms = () => {
    return arrFilm.map((phim, index) => {
      return (
        <div key={index} className="ml-2 mb-10">
          <Film_Flip phim={phim} />
        </div>
      );
    });
  };
  let activeClassDC = dangChieu ? "active_film" : "none_active_film";
  let activeClassSC = sapChieu ? "active_film" : "none_active_film";
  var settings = {
    dots: true,
    speed: 1000,
    slidesToShow: 5,
    slidesToScroll: 5,
    rows: 2,
    autoplay: true,
    infinite: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,

          dots: true,
        },
      },

      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          rows: 1,
          initialSlide: 20,
          dots: false,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          rows: 5,
          autoplay: false,
          dots: true,
        },
      },
    ],
  };
  return (
    <div className="multi multiItem">
      <button
        type="button"
        class={`${activeClassDC} hover:bg-blue-400  font-medium rounded-full text-sm px-5 py-2 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800`}
        onClick={() => {
          dispatch({ type: SET_FILM_DANG_CHIEU });
        }}
      >
        Phim đang chiếu
      </button>
      <button
        type="button"
        class={`${activeClassSC} hover:bg-red-300  font-medium rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900`}
        onClick={() => {
          dispatch({ type: SET_FILM_SAP_CHIEU });
        }}
      >
        Phim sắp chiếu
      </button>

      <Slider {...settings}>{renderFilms()}</Slider>
    </div>
  );
};
export default MultipleRowSlick;
