import _ from "lodash";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Logout from "../../../../components/Logout/Logout";
export default function Header() {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
  const renderLogin = () => {
    if (_.isEmpty(userLogin)) {
      return (
        <>
          <NavLink to={"/login"}>
            <button className="self-center px-8 py-3 rounded text-lg font-medium hover:text-red-400">
              Đăng nhập
            </button>
          </NavLink>
          <NavLink to={"/register"}>
            <button className="self-center px-8 py-3  rounded dark:bg-violet-400 dark:text-gray-900 text-lg font-medium hover:text-red-400">
              Đăng ký
            </button>
          </NavLink>
        </>
      );
    }
    return (
      <div className="flex">
        <NavLink to={"/profile"} className="pl-4">
          <div className="flex justify-around items-center">
            <div>
              <img
                className="rounded-full w-10"
                src="http://demo1.cybersoft.edu.vn/static/media/avatarTix.546c691f.jpg"
                alt=""
              />
            </div>
            <div className="text-gray-400 text-lg font-medium pl-2 border-r-2 pr-2 hover:text-red-400">
              {userLogin.hoTen}
            </div>
          </div>
        </NavLink>
        <Logout />
      </div>
    );
  };

  return (
    <header style={{ height: "88px" }}>
      <div
        className="fixed dark:bg-gray-800 dark:text-gray-100
      bg-black bg-opacity-40 text-white  w-full z-10"
      >
        <nav
          className="
    relative
    w-full
    flex flex-wrap
    items-center
    justify-between
    py-4
    bg-gray-100
    text-gray-500
    hover:text-gray-700
    focus:text-gray-700
    shadow-lg
    navbar navbar-expand-lg navbar-light
    "
        >
          <div className="container-fluid w-full flex flex-wrap items-center justify-between px-6">
            <img
              src="https://cyberlearn.vn/wp-content/uploads/2020/03/cyberlearn-min-new-opt2.png"
              alt=""
            />
            <button
              className="
          lg:hidden
  navbar-toggler
  text-gray-500
  border-0
  hover:shadow-none hover:no-underline
  py-2
  px-2.5
  bg-transparent
  focus:outline-none focus:ring-0 focus:shadow-none focus:no-underline
"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <svg
                aria-hidden="true"
                focusable="false"
                data-prefix="fas"
                data-icon="bars"
                className="w-6"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
              >
                <path
                  fill="currentColor"
                  d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"
                ></path>
              </svg>
            </button>

            <div
              className="collapse navbar-collapse flex-grow  items-center"
              id="navbarSupportedContent"
            >
              {/* <a className="text-xl text-black" href="#">
            Navbar
          </a> */}
              {/* Left links */}
              <ul className="navbar-nav flex flex-col pl-0 list-style-none mr-auto">
                <NavLink to={"/"}>
                  <li className="nav-item px-2">
                    <a className="nav-link active" aria-current="page" href="#">
                      Trang chủ
                    </a>
                  </li>
                </NavLink>
                <li className="nav-item pr-2">
                  <a
                    className="nav-link text-gray-500 hover:text-gray-700 focus:text-gray-700 p-0"
                    href="#cumrap"
                  >
                    Cụm rạp
                  </a>
                </li>
                <li className="nav-item pr-2">
                  <a
                    className="nav-link text-gray-500 hover:text-gray-700 focus:text-gray-700 p-0"
                    href="#news"
                  >
                    Tin tức
                  </a>
                </li>
                <li className="nav-item pr-2">
                  <a
                    className="nav-link text-gray-500 hover:text-gray-700 focus:text-gray-700 p-0"
                    href="#app"
                  >
                    Ứng dụng
                  </a>
                </li>
              </ul>
              {/* Left links */}
              <div className="">{renderLogin()}</div>
            </div>
            {/* Collapsible wrapper */}
          </div>
        </nav>
      </div>
    </header>
  );
}
