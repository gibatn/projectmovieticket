import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useDispatch, useSelector } from "react-redux";
import "./HomeCarousel.css";
import { getCarouselAction } from "../../../../redux/actions/CarouselAction";
import Slider from "react-slick";
const contentStyle = {
  // height: "90vh",
  // with: "100vw",
  // backgroundSize: "cover",
  // backgroundPosition: "center",
  // backgroundRepeat: "no-repeat",
};
export default function HomeCarousel() {
  const { arrImg } = useSelector((state) => state.CarouselReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCarouselAction);
  }, []);

  var settings = {
    dots: false,
    speed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,

    autoplay: true,
    infinite: true,
  };
  const renderImg = () => {
    return arrImg.map((item, index) => {
      return (
        <div>
          <div
            key={index}
            className="homecarousel__img-bg"
            style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}
          >
            <img
              src={item.hinhAnh}
              className="h-full w-full opacity-0"
              alt="123"
            />
          </div>
        </div>
      );
    });
  };
  return (
    // <Carousel
    //   autoplay
    //   infinite={true}
    //   dots={true}
    //   effect="fade"
    //   style={{ position: "relative", height: "100%" }}
    // >
    //   {renderImg()}
    // </Carousel>
    <Slider {...settings}>{renderImg()}</Slider>
  );
}
