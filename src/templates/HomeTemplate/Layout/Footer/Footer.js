import React from "react";
import { useSelector } from "react-redux";
import _ from "lodash";

import APP from "../../../../img/FooterImg/img/apple-logo.png";
import ADR from "../../../../img/FooterImg/img/android-logo.png";
import FAC from "../../../../img/FooterImg/img/facebook-logo.png";
import ZALG from "../../../../img/FooterImg/img/zalo-logo.png";
import "./Footer.css";
export default function Footer() {
  let { heThongRapChieu } = useSelector((state) => state.QuanLyRapReducer);
  const arrHeThongRap = _.map(heThongRapChieu, (heThongRap) =>
    _.pick(heThongRap, ["maHeThongRap", "tenHeThongRap", "logo"])
  );
  return (
    <footer class=" py-6 dark:bg-gray-800 dark:text-gray-50 ">
      <div class="container__footer px-6 mx-auto space-y-6 divide-y-2 divide-gray-400 md:space-y-12 divide-opacity-30">
        <div class="grid grid-cols-2 md:grid-cols-4">
          <div className="footer-top__content">
            <h3>TIX</h3>
            <p>FAQ</p>
            <p>Brand Guidelines</p>
          </div>
          <div className="footer-top__content">
            <h3>ĐỐI TÁC</h3>
            <div className="grid grid-cols-3 gap-y-5">
              {arrHeThongRap.map((item, index) => {
                return (
                  <div className="content__img " key={index}>
                    <img src={item.logo} width="25" alt="icon" />
                  </div>
                );
              })}
            </div>
          </div>
          <div className="footer-top__content">
            <h3>ỨNG DỤNG</h3>
            <div className="content__img ">
              <img src={APP} width="25" alt="icon" />
              <img src={ADR} width="25" alt="icon" />
            </div>
          </div>
          <div className="footer-top__content">
            <h3>LIÊN HỆ</h3>
            <div className="content__img  ">
              <img src={FAC} width="25" alt="icon" />
              <img src={ZALG} width="25" alt="icon" />
            </div>
          </div>
        </div>

        <div class="pt-6 text-center">
          <span className="text-gray-300 font-medium text-xl">
            ©2022 All rights reserved
          </span>
        </div>
      </div>
    </footer>
  );
}
