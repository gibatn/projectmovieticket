import React, { useEffect } from "react";
import Footer from "./Layout/Footer/Footer";
import Header from "./Layout/Header/Header";

export default function HomeTemplate({ Component }) {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <div className="">
      <Header />
      <Component />
      <hr className="m-2" />

      <Footer />
    </div>
  );
}
