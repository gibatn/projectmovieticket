import React, { useEffect } from "react";
import { USER_LOGIN } from "../../util/settings/config";
import { Navigate } from "react-router-dom";
// import HomeCarousel from "./Layout/HomeCarousel/HomeCarousel";

export default function CheckoutTemplate({ Component }) {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  let handleCheckout = () => {
    if (localStorage.getItem(USER_LOGIN)) {
      return (
        <div className="">
          {/* <Header /> */}
          <Component />
          <hr className="m-2" />
          {/* <Footer /> */}
        </div>
      );
    }
    return <Navigate to={"/login"} />;
  };
  return <div>{handleCheckout()}</div>;
}
