import React from "react";

export default function UserTemplate({ Component }) {
  return (
    <div className="">
      {/* <h1 className="bg-black h-10"> Đây là homepage</h1> */}
      <Component />
      <hr className="m-2" />
    </div>
  );
}
