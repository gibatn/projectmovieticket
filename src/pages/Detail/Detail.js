import React, { useEffect } from "react";
import { Progress, Rate } from "antd";
import style from "./Detail.module.css";
import { Tabs } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { NavLink, useParams } from "react-router-dom";
import { layThongTinChiTietPhim } from "../../redux/actions/QuanLyRapAction";
import moment from "moment";
import Popupvideo from "./PopupVideo";
const { TabPane } = Tabs;

export default function Detail(props) {
  let { id } = useParams();
  let { filmDetail } = useSelector((state) => state.QuanLyPhimReducer);

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(layThongTinChiTietPhim(id));
  }, []);

  return (
    <div className={`${style.movie__bg}`}>
      <div className={`${style.movie__detail}`}>
        <div className={`${style.detail__img} `}>
          <img
            src={`${filmDetail.hinhAnh}`}
            alt=""
            onError={(e) => (e.target.src = "https://picsum.photos/1000")}
          />
        </div>
        <div className="sm:pl-10   text-black space-y-3 ">
          <p className="text-white text-xl font-medium uppercase">
            {filmDetail.tenPhim}
          </p>
          <p
            style={{ fontSize: "16px" }}
            className="text-gray-400 leading-7 font-arial"
          >
            {filmDetail.moTa?.length > 200
              ? filmDetail.moTa?.slice(0, 200) + " ..."
              : filmDetail.moTa}
          </p>
          <div className={`${style.movie__detail__content}`}>
            <span className="font-bold ">Phân Loại</span>
            <span className="text-red-500 font-medium">
              C16 - PHIM DÀNH CHO KHÁN GIẢ TỪ 16 TUỔI TRỞ LÊN
            </span>
          </div>
          <div className={`${style.movie__detail__content}`}>
            <span className="font-bold ">Đạo diễn</span>
            <span className="text-gray-300">Kazuhisa Yusa</span>
          </div>
          <div className={`${style.movie__detail__content}`}>
            <span className="font-bold ">Khởi chiếu</span>
            <span className="text-gray-300">
              {moment(filmDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
            </span>
          </div>
          <div className={`${style.movie__detail__content}`}>
            <span className="font-bold ">Thời lượng</span>
            <span className="text-gray-300">72 phút</span>
          </div>
          <div className={`${style.movie__detail__content}`}>
            <span className="font-bold ">Ngôn ngữ</span>
            <span className="text-gray-300">Phụ đề tiếng việt</span>
          </div>
          <div className={`${style.movie__detail__content} hidden`}>
            <span className="font-bold ">Đánh giá</span>
            <span className="text-gray-300">10 điểm</span>
          </div>

          {/* modal video */}
          <Popupvideo trailer={filmDetail.trailer} />
        </div>
        <div className={`${style.movie__item__progress}`}>
          <Progress
            type="circle"
            strokeColor="green"
            percent={filmDetail.danhGia * 10}
            format={(num) => num / 10 + " điểm"}
          />
        </div>
      </div>
      <Tabs style={{ minHeight: 200 }} defaultActiveKey="1" centered>
        <TabPane
          tab={<p className="text-white font-medium text-xl">Lịch chiếu</p>}
          key="1"
        >
          <Tabs className=" mt-5" tabPosition={"left"}>
            {filmDetail.heThongRapChieu?.map((htr, index) => {
              return (
                <TabPane
                  tab={
                    <div>
                      <img src={htr.logo} width={50} />
                    </div>
                  }
                  key={index}
                >
                  {htr.cumRapChieu?.map((cumRap, index) => {
                    return (
                      <div key={index}>
                        <div className="flex text-white">
                          <div style={{ width: 300 }} className="mx-2 ">
                            <p className="text-md sm:text-xl ">
                              {cumRap.tenCumRap}
                            </p>
                            <p className="text-gray-400 ">{cumRap.diaChi}</p>
                          </div>
                          <div className="thong-tin-lich-chieu grid grid-cols-1 md:grid-cols-3 gap-1">
                            {cumRap.lichChieuPhim
                              ?.slice(0, 12)
                              .map((lichChieu, index) => {
                                return (
                                  <NavLink
                                    to={`/checkout/${lichChieu.maLichChieu}`}
                                    key={index}
                                  >
                                    <button
                                      className={`${style.movie__tab__btn}`}
                                    >
                                      <div>
                                        <span className="text-green-600">
                                          {moment(
                                            lichChieu.ngayChieuGioChieu
                                          ).format("DD-MM-YYYY")}
                                        </span>
                                        ~
                                        <span className="text-red-500">
                                          {moment(
                                            lichChieu.ngayChieuGioChieu
                                          ).format("hh:mm A")}
                                        </span>
                                      </div>
                                    </button>
                                  </NavLink>
                                );
                              })}
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
        <TabPane
          tab={<p className="text-white font-medium text-xl">Đánh giá</p>}
          key="2"
        >
          <p className="text-white"> Chưa có đánh giá nào</p>
        </TabPane>
      </Tabs>
    </div>
  );
}
