import React, { useEffect, useState } from "react";
import { USER_LOGIN } from "../../util/settings/config";
import { Button, Checkbox, Form, Input, InputNumber, message } from "antd";
import { Navigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  capNhatThongTinNguoiDungAction,
  layThongTinNguoiDungAction,
} from "../../redux/actions/QuanLyNguoiDungAction";
import { KetQuaDatVe } from "../Checkout/Checkout";
export default function Profile() {
  let { thongTinNguoiDung: user } = useSelector(
    (state) => state?.QuanLyNguoiDungReducer
  );
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(layThongTinNguoiDungAction());
  }, []);
  let handerForm = () => {
    if (user.taiKhoan !== undefined) {
      return (
        <Form
          name="basic"
          labelCol={{
            span: 12,
          }}
          wrapperCol={{
            span: 32,
          }}
          initialValues={{
            remember: false,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          onValuesChange={onFormLayoutChange}
        >
          <Form.Item
            label="Tài Khoản"
            name="taiKhoan"
            initialValue={user.taiKhoan}
          >
            <Input disabled />
          </Form.Item>

          <Form.Item
            label="Mật Khẩu"
            name="matKhau"
            initialValue={user?.matKhau}
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            label="Họ Tên"
            name="hoTen"
            value="222"
            initialValue={user.hoTen}
            rules={[
              {
                required: true,
                message: "Vui lòng điền họ tên!",
              },
            ]}
            hasFeedback
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Email "
            name="email"
            initialValue={user.email}
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
              {
                type: "email",
                message: "Vui lòng nhập email!",
              },
            ]}
            hasFeedback
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Số Điện Thoại"
            name="soDT"
            initialValue={user.soDT}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập số điện thoại!",
              },
              {
                pattern: new RegExp(/^[0-9\-\+]{9}$$/),
                message: "Số điện thoại chỉ 10 số!",
              },

              // {
              //   validator(_, value) {
              //     if (!value || typeof value === Number) {
              //       return Promise.resolve();
              //     }
              //     return Promise.reject("Vui lòng nhập số!");
              //   },
              // },
            ]}
            hasFeedback
          >
            <InputNumber className="w-full" />
          </Form.Item>
          <Form.Item
            label="Mã loại người dùng"
            name="maLoaiNguoiDung"
            initialValue={user.maLoaiNguoiDung}
          >
            <Input disabled />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" className="bg-blue-400" htmlType="submit">
              Cập nhật
            </Button>
          </Form.Item>
        </Form>
      );
    }
  };
  const onFinish = (values) => {
    values.maNhom = user.maNhom;
    dispatch(capNhatThongTinNguoiDungAction(values));
  };
  const onFinishFailed = (errorInfo) => {};
  const [componentDisabled, setComponentDisabled] = useState(true);
  const onFormLayoutChange = ({ disabled }) => {
    setComponentDisabled(disabled);
  };
  let handleProfile = () => {
    if (localStorage.getItem(USER_LOGIN)) {
      return (
        <div className="w-screen  pt-20">
          <div className="md:flex items-start">
            <ul
              className="nav nav-tabs flex flex-col flex-wrap list-none border-b-0 pl-0 mr-4"
              id="tabs-tabVertical"
              role="tablist"
            >
              <li
                className="nav-item flex-grow text-center"
                role="presentation"
              >
                <a
                  href="#tabs-homeVertical"
                  className="
    nav-link
    block
    font-medium
    text-xs
    leading-tight
    uppercase
    border-x-0 border-t-0 border-b-2 border-transparent
    px-6
    py-3
    my-2
    hover:border-transparent hover:bg-gray-100
    focus:border-transparent
    active
  "
                  id="tabs-home-tabVertical"
                  data-bs-toggle="pill"
                  data-bs-target="#tabs-homeVertical"
                  role="tab"
                  aria-controls="tabs-homeVertical"
                  aria-selected="true"
                >
                  TÀI KHOẢN
                </a>
              </li>
              <li
                className="nav-item flex-grow text-center"
                role="presentation"
              >
                <a
                  href="#tabs-profileVertical"
                  className="
    nav-link
    block
    font-medium
    text-xs
    leading-tight
    uppercase
    border-x-0 border-t-0 border-b-2 border-transparent
    px-6
    py-3
    my-2
    hover:border-transparent hover:bg-gray-100
    focus:border-transparent
  "
                  id="tabs-profile-tabVertical"
                  data-bs-toggle="pill"
                  data-bs-target="#tabs-profileVertical"
                  role="tab"
                  aria-controls="tabs-profileVertical"
                  aria-selected="false"
                >
                  Lịch sử đặt vé
                </a>
              </li>
            </ul>
            <div
              className="tab-content px-10 w-full"
              id="tabs-tabContentVertical"
            >
              <div
                className="tab-pane fade show active w-1/2"
                id="tabs-homeVertical"
                role="tabpanel"
                aria-labelledby="tabs-home-tabVertical"
              >
                {/* Tab 1 content vertical */}
                <div className="flex justify-center pb-5">
                  <Checkbox
                    checked={componentDisabled}
                    onChange={(e) => setComponentDisabled(e.target.checked)}
                  >
                    Thay đổi thông tin
                  </Checkbox>
                </div>
                {/* from */}
                {handerForm()}
              </div>
              <div
                className="tab-pane fade"
                id="tabs-profileVertical"
                role="tabpanel"
                aria-labelledby="tabs-profile-tabVertical"
              >
                <div className="kqdatve p-0 -mt-20">{<KetQuaDatVe />}</div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return <Navigate to={"/login"} />;
  };
  return <>{handleProfile()}</>;
}
