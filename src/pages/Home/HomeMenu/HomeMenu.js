import React, { Fragment, memo } from "react";
import { Tabs } from "antd";
import { NavLink } from "react-router-dom";
import moment from "moment";
import "./HomeMenu.css";
const { TabPane } = Tabs;

const onChange = (key) => {};
function HomeMenu({ heThongRapChieu }) {
  const renderHeThongRap = () => {
    return heThongRapChieu?.map((heThongRap, index) => {
      return (
        <TabPane
          tab={
            <img src={heThongRap.logo} className=" rounded-full " width="50" />
          }
          key={index}
        >
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
          >
            {heThongRap.lstCumRap?.map((cumRap, index) => {
              return (
                <TabPane
                  className="border-2"
                  tab={
                    <div className="text-left " style={{ width: 300 }}>
                      <p className="text-xl text-green-500 truncate">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="text-gray-400 truncate">{cumRap.diaChi}</p>
                      <button className="text-red-500 font-medium">
                        [chi tiết]
                      </button>
                    </div>
                  }
                  key={cumRap.tenCumRap}
                >
                  {/* Load phim */}

                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="shadow-lg"
                  >
                    {cumRap.danhSachPhim
                      .filter((item) => item.hinhAnh !== "")
                      .map((phim, index) => {
                        return (
                          <div
                            className="overflow-hidden"
                            style={{ maxHeight: 200 }}
                            key={index}
                          >
                            <div className="flex items-center  py-4">
                              <div>
                                <img
                                  className="rounded-lg"
                                  style={{ height: 100, width: 100 }}
                                  src={phim.hinhAnh}
                                  alt={phim.tenPhim}
                                  onError={(e) =>
                                    (e.target.src =
                                      "https://picsum.photos/1000")
                                  }
                                />
                              </div>
                              <div className="ml-2 space-y-2">
                                <h1 className="text-2xl text-black ml-3">
                                  {phim.tenPhim}
                                </h1>

                                <div className="grid grid-cols-3 lg:grid-cols-2 gap-1 font-medium overflow-hidden">
                                  {phim.lstLichChieuTheoPhim
                                    ?.slice(0, 6)
                                    .map((lichChieu, index) => {
                                      return (
                                        <NavLink
                                          to={`/checkout/${lichChieu.maLichChieu}`}
                                          className="text-orange-400 mx-2 rounded-md"
                                          key={index}
                                        >
                                          <button className="py-2 px-2 bg-gray-100 rounded-lg hover:scale-105 font-bold transiton duration-300">
                                            <span className="text-green-600">
                                              {moment(
                                                lichChieu.ngayChieuGioChieu
                                              ).format("DD-MM-YYYY")}
                                            </span>
                                            ~
                                            <span className="text-red-500">
                                              {moment(
                                                lichChieu.ngayChieuGioChieu
                                              ).format("hh:mm A")}
                                            </span>
                                          </button>
                                        </NavLink>
                                      );
                                    })}
                                </div>
                              </div>
                            </div>
                            <hr />
                          </div>
                        );
                      })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  return (
    <div className="menu-tab w-11/12 mx-auto">
      <Tabs tabPosition="left" defaultActiveKey="1" onChange={onChange}>
        {renderHeThongRap()}
      </Tabs>
    </div>
  );
}
export default memo(HomeMenu);
