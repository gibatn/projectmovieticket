import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import MultipleRowSlick from "../../components/RSlick/MultipleRowSlick";
import { layDanhSachPhimAction } from "../../redux/actions/QuanLyFilmActions";
import { layDanhSachHeThongRapAction } from "../../redux/actions/QuanLyRapAction";
import HomeCarousel from "../../templates/HomeTemplate/Layout/HomeCarousel/HomeCarousel";
import MovieApp from "../MovieApp/MovieApp";
import News from "../News/News";
import HomeMenu from "./HomeMenu/HomeMenu";
export default function Home() {
  const { arrFilm } = useSelector((state) => state.QuanLyPhimReducer);
  const { heThongRapChieu } = useSelector((state) => state.QuanLyRapReducer);
  // let renderFilms = () => {
  //   return arrFilm.map((phim, index) => {
  //     return (
  //       <div key={index}>
  //         <Film key={index} phim={phim} />
  //       </div>
  //     );
  //   });
  // };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachPhimAction());
    dispatch(layDanhSachHeThongRapAction());
  }, []);

  return (
    <div className="">
      <HomeCarousel />
      <div className="container mx-auto">
        <section class="text-gray-600 body-font">
          <div class="container px-5 py-10 mx-auto ">
            <MultipleRowSlick arrFilm={arrFilm} />
          </div>
        </section>

        <HomeMenu heThongRapChieu={heThongRapChieu} />
      </div>
      <News />
      <MovieApp />
    </div>
  );
}
