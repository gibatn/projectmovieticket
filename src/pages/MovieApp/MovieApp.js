import React from "react";
import { Carousel } from "antd";

import "./MovieApp.css";

import slide1 from "../../img/BlockAppImg/img/slide1.jpg";
import slide2 from "../../img/BlockAppImg/img/slide2.jpg";
import slide3 from "../../img/BlockAppImg/img/slide3.jpg";
import slide4 from "../../img/BlockAppImg/img/slide4.jpg";
import slide5 from "../../img/BlockAppImg/img/slide5.jpg";
const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
export default function MovieApp() {
  return (
    <div id="app" className="movieApp">
      <div className="container__app grid md:grid-cols-2 grid-cols-1 gap-10">
        <div className="app__content space-y-5 md:space-y-7 text-red-500">
          <h3>Ứng dụng tiện lợi dành cho người yêu điện ảnh</h3>
          <p>
            Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và
            đổi quà hấp dẫn.
          </p>
          <button className="movieApp__Btn">App miễn phí - Tải về ngay!</button>
          <p>TIX có hai phiên bản iOS & Android</p>
        </div>
        <div>
          <Carousel autoplay dots={false} effect="fade">
            <div className="imgApp__carousel">
              <img src={slide1} alt="" />
            </div>
            <div className="imgApp__carousel">
              <img src={slide2} alt="" />
            </div>
            <div className="imgApp__carousel">
              <img src={slide3} alt="" />
            </div>
            <div className="imgApp__carousel">
              <img src={slide4} alt="" />
            </div>
            <div className="imgApp__carousel">
              <img src={slide5} alt="" />
            </div>
          </Carousel>
        </div>
      </div>
    </div>
  );
}
