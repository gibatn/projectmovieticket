import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Switch,
  TreeSelect,
} from "antd";
import { useField, useFormik } from "formik";
import moment from "moment/moment";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { themPhimUploadHinh } from "../../../../redux/actions/QuanLyFilmActions";
import { quanLyPhimService } from "../../../../services/QuanLyPhimService";
import { GROUPID } from "../../../../util/settings/config";
import * as yup from "yup";
const AddNew = () => {
  const dispatch = useDispatch();
  const [imgSrc, setImgSrc] = useState("");
  let valiSchema = yup.object().shape({
    tenPhim: yup
      .string()
      .required("Vui lòng nhập tên phim!")
      .min(5, "Tên phim phải trên 5 ký tự!"),
    trailer: yup
      .string()
      .required("Vui lòng nhập trailer phim!")
      .matches("https://", "Trailer phải bắt đầu bằng https://"),
    moTa: yup
      .string()
      .required("Vui lòng nhập mô tả phim!")
      .min(10, "Tên phim phải trên 10 ký tự!"),
    ngayKhoiChieu: yup.string().required("Vui chọn ngày khởi chiếu phim!"),
    danhGia: yup.string().required("Vui lòng nhập đánh giá phim!").nullable(),
    hinhAnh: yup
      .mixed()
      .test("fileFormat", "Vui lòng chọn hình ảnh phim", (value) => {
        return value && value.type !== undefined;
      }),
  });
  const formik = useFormik({
    initialValues: {
      tenPhim: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      dangChieu: false,
      sapChieu: false,
      hot: false,
      danhGia: 0,
      hinhAnh: {},
    },
    validationSchema: valiSchema,
    onSubmit: (values) => {
      let formData = new FormData();
      for (let value in values) {
        if (value !== "hinhAnh") {
          formData.append(value, values[value]);
        } else {
          formData.append("File", values.hinhAnh, values.hinhAnh.name);
        }
      }

      dispatch(themPhimUploadHinh(formData));
    },
  });

  let handleChangeDatePicker = (values) => {
    let ngayKhoiChieu = moment(values).format("DD/MM/YYYY");
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  let handleChangeSwitch = (name) => {
    return (value) => formik.setFieldValue(name, value);
  };

  let handleChangeFile = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      setImgSrc(e.target.result);
    };
    formik.setFieldValue("hinhAnh", file);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
    >
      <h3 className="text-center pb-5 text-2xl text-blue-500">Thêm phim</h3>
      <Form.Item label="Tên phim">
        <Input name="tenPhim" onChange={formik.handleChange} />
        <p className="text-red-500">{formik.errors.tenPhim}</p>
      </Form.Item>

      <Form.Item label="Trailer">
        <Input name="trailer" onChange={formik.handleChange} />
        <p className="text-red-500">{formik.errors.trailer}</p>
      </Form.Item>

      <Form.Item label="Mô tả">
        <Input name="moTa" onChange={formik.handleChange} />
        <p className="text-red-500">{formik.errors.moTa}</p>
      </Form.Item>

      <Form.Item label="Ngày khởi chiếu">
        <DatePicker format={"DD/MM/YYYY"} onChange={handleChangeDatePicker} />
        <p className="text-red-500">{formik.errors.ngayKhoiChieu}</p>
      </Form.Item>

      <Form.Item label="Đang chiếu">
        <Switch
          name="dangChieu"
          className="bg-blue-400"
          onChange={handleChangeSwitch("dangChieu")}
        />
      </Form.Item>

      <Form.Item label="Sắp chiếu">
        <Switch
          name="sapChieu"
          className="bg-blue-400"
          onChange={handleChangeSwitch("sapChieu")}
        />
      </Form.Item>

      <Form.Item label="Hot">
        <Switch
          name="hot"
          className="bg-blue-400"
          onChange={handleChangeSwitch("hot")}
        />
      </Form.Item>

      <Form.Item label="Số sao">
        <InputNumber
          min={1}
          max={10}
          onChange={(values) => formik.setFieldValue("danhGia", values)}
        />
        <p className="text-red-500">{formik.errors.danhGia}</p>
      </Form.Item>
      <Form.Item label="Hình ảnh">
        <input type="file" onChange={handleChangeFile} accept="image/*" />
        <img
          style={{ width: 150, height: 150, paddingTop: "10px" }}
          src={imgSrc}
          alt="..."
        />
        <p className="text-red-500">{formik.errors.hinhAnh}</p>
      </Form.Item>

      <Form.Item label="Tác vụ">
        <Button htmlType="submit" className="bg-blue-400">
          Thêm phim
        </Button>
      </Form.Item>
    </Form>
  );
};
export default AddNew;
