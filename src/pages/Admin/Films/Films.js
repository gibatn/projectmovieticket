import React, { Fragment, useEffect } from "react";
import { Button, Table } from "antd";
import {
  AudioOutlined,
  CalendarOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Input, Space } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  layDanhSachPhimAction,
  xoaPhimAction,
} from "../../../redux/actions/QuanLyFilmActions";
import { NavLink, useNavigate } from "react-router-dom";

const onChange = (pagination, filters, sorter, extra) => {
  console.log("params", pagination, filters, sorter, extra);
};
export default function Films() {
  let history = useNavigate();
  const { arrFilmDefault } = useSelector((state) => state.QuanLyPhimReducer);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachPhimAction());
  }, []);
  const { Search } = Input;
  const columns = [
    {
      title: "mã phim",
      dataIndex: "maPhim",
      width: 100,
      // specify the condition of filtering result
      // here is that finding the name started with `value`

      sorter: (a, b) => a.maPhim - b.maPhim,
      sortDirections: ["descend"],
    },
    {
      title: "hình ảnh",
      width: 50,
      dataIndex: "hinhAnh",
      render: (value, film, index) => (
        <img
          width={50}
          src={value}
          onError={(e) => {
            e.target.onError = null;
            e.target.src = `https://picsum.photos/id/${index}/50/50`;
          }}
        />
      ),
    },
    {
      title: "tên phim",
      dataIndex: "tenPhim",
      width: 300,
      defaultSortOrder: "descend",
      sorter: (a, b) => {
        let tenPhimA = a.tenPhim.toLowerCase().trim();
        let tenPhimB = b.tenPhim.toLowerCase().trim();
        if (tenPhimA > tenPhimB) {
          return 1;
        }
        return -1;
      },
    },

    {
      title: "mô tả",
      dataIndex: "moTa",
      width: 300,
      defaultSortOrder: "descend",

      render: (value) => (
        <Fragment>
          {value.length > 50 ? value.slice(0, 50) + "..." : value}
        </Fragment>
      ),
    },
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      width: 300,
      defaultSortOrder: "descend",

      render: (text, film, index) => (
        <Fragment>
          <NavLink
            key={1}
            to={`/admin/film/edit/${film.maPhim}`}
            className="mr-2 text-3xl"
          >
            <EditOutlined style={{ color: "blue" }} />
          </NavLink>
          <button
            onClick={() => {
              if (window.confirm("Bạn có chắc muốn xóa phim " + film.tenPhim)) {
                dispatch(xoaPhimAction(film.maPhim));
              }
            }}
            key={2}
            className="text-3xl"
          >
            <DeleteOutlined style={{ color: "red" }} />
          </button>
          <NavLink
            className="text-3xl"
            to={`/admin/film/showtime/${film.maPhim}/${film.tenPhim}`}
          >
            <CalendarOutlined style={{ color: "blue" }} />
          </NavLink>
        </Fragment>
      ),
    },
  ];

  const data = arrFilmDefault;
  const onSearch = (value) => {
    dispatch(layDanhSachPhimAction(value));
  };
  return (
    <div className="space-y-5">
      <h3 className="text-4xl">Quản lý phim</h3>
      <Button
        onClick={() => {
          history("/admin/film/addnew");
        }}
        className=""
      >
        Thêm phim
      </Button>
      <Search
        className=""
        placeholder="input search text"
        size="large"
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey={"maPhim"}
      />
    </div>
  );
}
