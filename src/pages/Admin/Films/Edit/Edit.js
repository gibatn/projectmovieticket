import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Switch,
  TreeSelect,
} from "antd";
import { useFormik } from "formik";
import moment from "moment/moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  capNhatPhimUploadAction,
  layThongTinPhimAction,
} from "../../../../redux/actions/QuanLyFilmActions";
import * as yup from "yup";
const Edit = () => {
  let { id } = useParams();
  useEffect(() => {
    dispatch(layThongTinPhimAction(id));
  }, []);

  let { filmInfo } = useSelector((state) => state.QuanLyPhimReducer);
  const dispatch = useDispatch();
  const [imgSrc, setImgSrc] = useState(filmInfo.hinhAnh);
  useEffect(() => {
    setImgSrc(filmInfo.hinhAnh);
  }, [filmInfo.hinhAnh]);

  let valiSchema = yup.object().shape({
    tenPhim: yup
      .string()
      .required("Vui lòng nhập tên phim!")
      .min(5, "Tên phim phải trên 5 ký tự!"),
    trailer: yup
      .string()
      .required("Vui lòng nhập trailer phim!")
      .matches("https://", "Trailer phải bắt đầu bằng https://"),
    moTa: yup
      .string()
      .required("Vui lòng nhập mô tả phim!")
      .min(10, "Tên phim phải trên 10 ký tự!"),
    ngayKhoiChieu: yup.string().required("Vui chọn ngày khởi chiếu phim!"),
    danhGia: yup.string().required("Vui lòng nhập đánh giá phim!").nullable(),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      maPhim: filmInfo.maPhim,
      tenPhim: filmInfo.tenPhim,
      trailer: filmInfo.trailer,
      moTa: filmInfo.moTa,
      ngayKhoiChieu: filmInfo.ngayKhoiChieu,
      dangChieu: filmInfo.dangChieu,
      sapChieu: filmInfo.sapChieu,
      hot: filmInfo.hot,
      danhGia: filmInfo.danhGia,
      hinhAnh: null,
    },
    validationSchema: valiSchema,
    onSubmit: (values) => {
      let formData = new FormData();
      for (let value in values) {
        if (value !== "hinhAnh") {
          formData.append(value, values[value]);
        } else {
          if (values.hinhAnh !== null) {
            formData.append("File", values.hinhAnh, values.hinhAnh.name);
          }
        }
      }

      dispatch(capNhatPhimUploadAction(formData));
    },
  });
  let handleChangeDatePicker = (values) => {
    let ngayKhoiChieu = moment(values).format("DD/MM/YYYY");
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  let handleChangeSwitch = (name) => {
    return (value) => formik.setFieldValue(name, value);
  };

  let handleChangeFile = async (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      setImgSrc(e.target.result);
    };
    await formik.setFieldValue("hinhAnh", file);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
    >
      <h3 className="text-center pb-5 text-2xl text-blue-500">Cập nhật</h3>

      <Form.Item label="Tên phim">
        <Input
          name="tenPhim"
          value={formik.values.tenPhim}
          onChange={formik.handleChange}
        />
        <p className="text-red-500">{formik.errors.tenPhim}</p>
      </Form.Item>
      <Form.Item label="Trailer">
        <Input
          name="trailer"
          value={formik.values.trailer}
          onChange={formik.handleChange}
        />
        <p className="text-red-500">{formik.errors.trailer}</p>
      </Form.Item>
      <Form.Item label="Mô tả">
        <Input
          name="moTa"
          value={formik.values.moTa}
          onChange={formik.handleChange}
        />
        <p className="text-red-500">{formik.errors.moTa}</p>
      </Form.Item>

      <Form.Item label="Ngày khởi chiếu">
        <DatePicker
          format={"DD/MM/YYYY"}
          value={moment(formik.values.ngayKhoiChieu, "YYYY/MM/DD")}
          onChange={handleChangeDatePicker}
        />
        <p className="text-red-500">{formik.errors.ngayKhoiChieu}</p>
      </Form.Item>

      <Form.Item label="Đang chiếu">
        <Switch
          name="dangChieu"
          className="bg-blue-400"
          checked={formik.values.dangChieu}
          onChange={handleChangeSwitch("dangChieu")}
        />
      </Form.Item>

      <Form.Item label="Sắp chiếu">
        <Switch
          name="sapChieu"
          className="bg-blue-400"
          checked={formik.values.sapChieu}
          onChange={handleChangeSwitch("sapChieu")}
        />
      </Form.Item>

      <Form.Item label="Hot">
        <Switch
          name="hot"
          className="bg-blue-400"
          checked={formik.values.hot}
          onChange={handleChangeSwitch("hot")}
        />
      </Form.Item>

      <Form.Item label="Số sao">
        <InputNumber
          min={1}
          max={10}
          value={formik.values.danhGia}
          onChange={(values) => formik.setFieldValue("danhGia", values)}
        />
        <p className="text-red-500">{formik.errors.danhGia}</p>
      </Form.Item>
      <Form.Item label="Hình ảnh">
        <input type="file" onChange={handleChangeFile} accept="image/*" />
        <img
          style={{ width: 150, height: 150, paddingTop: "10px" }}
          src={imgSrc}
          alt="..."
        />
      </Form.Item>

      <Form.Item label="Tác vụ">
        <Button htmlType="submit" className="bg-blue-400">
          Cập nhật
        </Button>
      </Form.Item>
    </Form>
  );
};
export default Edit;
