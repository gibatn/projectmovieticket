import React, { useEffect, useState } from "react";
import { Button, Form, DatePicker, Select, InputNumber, message } from "antd";
import { quanLyRapService } from "../../../services/QuanLyRapService";
import { useFormik } from "formik";
import { useParams } from "react-router-dom";
import moment from "moment/moment";
import { quanLyDatVeService } from "../../../services/QuanLyDatVeService";
import { createRef } from "react";
export default function ShowTime() {
  let { id, tenPhim } = useParams();
  let formRef = createRef();
  console.log("12", tenPhim);
  const onFinish = (values) => {
    values.maPhim = id;
    values.ngayChieuGioChieu = moment(values.ngayChieuGioChieu).format(
      "DD/MM/YYYY hh:mm:ss"
    );
    values.maRap = values.cumRap;
    delete values.heThongRap;
    delete values.cumRap;
    console.log("Success:", values);
    quanLyDatVeService
      .taoLichChieu(values)
      .then((res) => {
        message.success("Tạo lịch chiếu thành công");
        console.log(res);
        formRef.current.resetFields();
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  // const formik = useFormik({
  //   initialValues: {
  //     maPhim: id,
  //     ngayChieuGioChieu: "",
  //     maRap: "",
  //     giaVe: "",
  //   },
  //   onSubmit: async (values) => {
  //     try {
  //       let result = await quanLyDatVeService.taoLichChieu(values);
  //       console.log(result);
  //       alert("Tạo lịch chiếu thành công");
  //     } catch (error) {
  //       console.log(error.response);
  //     }
  //   },
  // });
  const handleChangeHeThongRap = async (values) => {
    try {
      let result = await quanLyRapService.layThongTinCumRap(values);
      setstate({ ...state, cumRapChieu: result.data.content });
    } catch (error) {}
  };

  const handleChangeCumRap = (value) => {
    // formik.setFieldValue("maRap", value);
  };

  const onChangeDate = (date, dateString) => {
    console.log(date, dateString);
    // formik.setFieldValue("ngayChieuGioChieu", dateString);
  };
  const onChangeInputNumber = (value) => {
    console.log(value);
    // formik.setFieldValue("giaVe", value);
  };
  const [state, setstate] = useState({
    heThongRapChieu: [],
    cumRapChieu: [],
  });
  useEffect(async () => {
    try {
      let result = await quanLyRapService.layThongTinHeThongRap();
      setstate({ ...state, heThongRapChieu: result.data.content });
    } catch (error) {
      console.log(error);
    }
  }, []);

  return (
    <div>
      <Form
        // onSubmitCapture={formik.handleSubmit}
        ref={formRef}
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <h3 className="text-2xl text-center uppercase pb-5">
          Tạo lịch chiếu : {tenPhim}
        </h3>
        <Form.Item
          label="Hệ thống rạp"
          name="heThongRap"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn hệ thống rạp!",
            },
          ]}
        >
          <Select
            options={state.heThongRapChieu.map((item) => {
              return {
                value: item.maHeThongRap,
                label: item.tenHeThongRap,
              };
            })}
            placeholder="Chọn hệ thống rạp"
            onChange={handleChangeHeThongRap}
          />
        </Form.Item>
        <Form.Item
          label="Cụm rạp"
          name="cumRap"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn cum rạp!",
            },
          ]}
        >
          <Select
            options={state.cumRapChieu.map((item) => ({
              label: item.tenCumRap,
              value: item.maCumRap,
            }))}
            placeholder="Chọn cụm rạp"
            onChange={handleChangeCumRap}
          />
        </Form.Item>
        <Form.Item
          name="ngayChieuGioChieu"
          label="Ngày chiếu giờ chiếu"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn ngày giờ chiếu phim!",
            },
          ]}
        >
          <DatePicker
            name="ngayChieuGioChieu"
            showTime
            format={"DD/MM/YYYY hh:mm:ss"}
            onChange={onChangeDate}
          />
        </Form.Item>
        <Form.Item
          label="Giá vé"
          name="giaVe"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập giá vé!",
            },
          ]}
        >
          <InputNumber
            min={75000}
            max={150000}
            onChange={onChangeInputNumber}
          />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button className="bg-blue-400" type="primary" htmlType="submit">
            Thêm lịch chiếu
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
