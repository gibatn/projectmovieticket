import React, { Fragment, useEffect } from "react";
import { Tabs } from "antd";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useParams } from "react-router-dom";
import {
  datVeAction,
  layChiTietPhongVeAction,
} from "../../redux/actions/QuanLyDatVeAction";
import { CloseOutlined, UserOutlined, HomeOutlined } from "@ant-design/icons";
import style from "./Checkout.module.css";
import "./Checkout.css";
import { DAT_VE } from "../../redux/actions/types/QuanLyDatVeType";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { layThongTinNguoiDungAction } from "../../redux/actions/QuanLyNguoiDungAction";
import moment from "moment";
import { DANG_XUAT } from "../../redux/actions/types/QuanLyNguoiDungType";
import { USER_LOGIN } from "../../util/settings/config";
import Logout from "../../components/Logout/Logout";
function Checkout() {
  let { id } = useParams();
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
  const { chiTietPhongVe, danhSachGheDangDat } = useSelector(
    (state) => state.QuanLyDatVeReducer
  );
  const { thongTinPhim, danhSachGhe } = chiTietPhongVe;
  const dispatch = useDispatch();

  useEffect(() => {
    const action = layChiTietPhongVeAction(id);
    dispatch(action);
  }, []);
  const renderSeats = () => {
    return danhSachGhe.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      let classGheDangDat = "";
      let classGheDaDuocDat = "";

      if (userLogin.taiKhoan === ghe.taiKhoanNguoiDat) {
        classGheDaDuocDat = "gheDaDuocDat";
      }
      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === ghe.maGhe
      );
      if (indexGheDD != -1) {
        classGheDangDat = "gheDangDat";
      }
      return (
        <Fragment key={index}>
          <button
            onClick={() => {
              dispatch({
                type: DAT_VE,
                gheDuocChon: ghe,
              });
            }}
            disabled={ghe.daDat}
            className={`ghe ${classGheDangDat} ${classGheVip} ${classGheDaDat} ${classGheDaDuocDat}`}
          >
            {ghe.daDat ? (
              classGheDaDuocDat != "" ? (
                <UserOutlined style={{ marginBottom: 1, fontWeight: "bold" }} />
              ) : (
                <CloseOutlined
                  style={{ marginBottom: 1, fontWeight: "bold" }}
                />
              )
            ) : (
              ghe.stt
            )}
          </button>
          {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };
  const handleBuyTicket = () => {
    if (userLogin.taiKhoan) {
      const thongTinDatVe = new ThongTinDatVe();
      thongTinDatVe.maLichChieu = id;
      thongTinDatVe.danhSachVe = danhSachGheDangDat;
      dispatch(datVeAction(thongTinDatVe));
    } else {
    }
  };
  return (
    <div className=" mt-5">
      <div className="grid grid-cols-12 space-x-2">
        <div className="col-span-12 lg:col-span-9">
          <div className="flex flex-col justify-center items-center mt-5">
            <div className="text-center mb-5" id={`${style.trapezoid}`}>
              <h3 className="mt-3 text-black font-medium text-2xl">Màn hình</h3>
            </div>
            <div
              className="md:flex justify-center"
              style={{ width: "100%", overflowX: "scroll" }}
            >
              <div style={{ minWidth: "730px" }}>{renderSeats()}</div>
            </div>
          </div>
          <div className="mt-5 flex justify-center ">
            <table className="w-2/3 divide-y divide-gray-200 ">
              <thead>
                <th>Ghế chưa đặt</th>
                <th>Ghế đang đặt</th>
                <th>Ghế vip</th>
                <th>Ghế đã đặt</th>
                <th>Ghế mình đặt</th>
              </thead>
              <tbody className="text-center">
                <tr>
                  <td>
                    <button className="ghe "></button>
                  </td>
                  <td>
                    <button className="ghe gheDangDat "></button>
                  </td>
                  <td>
                    <button className="ghe gheVip "></button>
                  </td>
                  <td>
                    <button className="ghe gheDaDat ">
                      <CloseOutlined
                        style={{ marginBottom: 2, fontWeight: "bold" }}
                      />
                    </button>
                  </td>
                  <td>
                    <button className="ghe gheDaDuocDat ">
                      <UserOutlined
                        style={{ marginBottom: 2, fontWeight: "bold" }}
                      />
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className="col-span-12 lg:col-span-3 mx-auto">
          <h3 className="text-green-400 text-center text-4xl pb-5">
            <span className="text-green-500">
              {danhSachGheDangDat
                .reduce((tongTien, ghe, index) => {
                  return (tongTien += ghe.giaVe);
                }, 0)
                .toLocaleString()}
              VND
            </span>
          </h3>

          <div className="content__right__checkout">
            <span className="">Tên Phim: </span>
            <span className="">{thongTinPhim.tenPhim}</span>
          </div>
          <div className="content__right__checkout">
            <span className=""> Cụm Rạp: </span>
            <span className="">{thongTinPhim.tenCumRap}</span>
          </div>
          <div className="content__right__checkout">
            <span className="">Rạp: </span>
            <span className="">{thongTinPhim.tenRap}</span>
          </div>
          <div className="content__right__checkout">
            <span className="">Ngày giờ chiếu :</span>
            <span className="">
              {thongTinPhim.ngayChieu} - {thongTinPhim.gioChieu}
            </span>
          </div>

          <hr />
          <div className="content__right__checkout">
            <span> Ghế</span>

            <span className="grid grid-cols-4">
              {danhSachGheDangDat.map((gheDD, index) => {
                return (
                  <span className="" key={index}>
                    Ghế {gheDD.stt}
                  </span>
                );
              })}
            </span>
          </div>

          <div className="mb-0 h-full flex flex-col  items-center">
            <div
              onClick={handleBuyTicket}
              className="bg-green-500 text-white text-center text-2xl w-full py-3 font-bold rounded cursor-pointer"
            >
              Đặt vé
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default function CheckOutTab(props) {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);

  const operations = () => {
    return (
      <Fragment>
        {!_.isEmpty(userLogin) ? (
          <>
            <div className="flex">
              <NavLink to={"/profile"} className="pl-4">
                <div className="flex justify-around items-center">
                  <div>
                    <img
                      className="rounded-full w-10"
                      src="http://demo1.cybersoft.edu.vn/static/media/avatarTix.546c691f.jpg"
                      alt=""
                    />
                  </div>
                  <div className="text-gray-400 text-lg font-medium pl-2 border-r-2 pr-2">
                    {userLogin.hoTen}
                  </div>
                </div>
              </NavLink>
              <Logout />
            </div>
          </>
        ) : (
          ""
        )}
      </Fragment>
    );
  };
  const { tabActive } = useSelector((state) => state.QuanLyDatVeReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: "CHUYEN_TAB12" });
  }, []);

  return (
    <div className="p-5">
      <Tabs
        tabBarExtraContent={operations()}
        defaultActiveKey="2"
        activeKey={tabActive}
        onChange={(key) => {
          dispatch({
            type: "CHANGE_TAB_ACTIVE",
            number: key,
          });
        }}
      >
        <Tabs.TabPane tab="1: Chọn ghế thanh toán" key="1">
          <Checkout />
        </Tabs.TabPane>

        <Tabs.TabPane tab="2: Kết quả đặt vé" key="2">
          <KetQuaDatVe {...props} />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <NavLink to={"/"}>
              <HomeOutlined
                className="text-2xl text-center"
                style={{ margin: 15 }}
              />
            </NavLink>
          }
          key="3"
        ></Tabs.TabPane>
      </Tabs>
    </div>
  );
}
export function KetQuaDatVe() {
  let dispatch = useDispatch();
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
  const { thongTinNguoiDung } = useSelector(
    (state) => state.QuanLyNguoiDungReducer
  );
  useEffect(() => {
    dispatch(layThongTinNguoiDungAction());
  }, []);
  const renderTicketItem = () => {
    return thongTinNguoiDung.thongTinDatVe?.map((ticket, index) => {
      const seats = _.first(ticket.danhSachGhe);
      return (
        <div className="p-2 lg:w-1/3 sm:w-1/2 w-full" key={index}>
          <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
            <img
              alt="team"
              className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4 hidden md:block"
              src={ticket.hinhAnh}
              onError={(e) => (
                (e.target.onerror = null),
                (e.target.src = "https://picsum.photos/200/200")
              )}
            />
            <div className="flex-grow">
              <h2 className="text-green-500 title-font text-xl font-medium pb-4">
                {ticket.tenPhim}
              </h2>
              <div className="content__card__checkout ">
                <span>Ngày chiếu:</span>
                <span>
                  {moment(ticket.ngayDat).format(" DD-MM-YYYY")}~
                  {moment(ticket.ngayDat).format("hh:mm A")}
                </span>
              </div>
              <div className="content__card__checkout ">
                <span>Địa điểm:</span>
                <span> {seats.tenHeThongRap}</span>
              </div>
              <div className="content__card__checkout ">
                <span>Tên rạp:</span>
                <span>{seats.tenCumRap}</span>
              </div>
              <div className="content__card__checkout ">
                <span>Ghế :</span>
                <span>
                  {ticket.danhSachGhe.slice(0, 15).map((ghe, index) => {
                    return (
                      <Fragment key={index}>
                        <span>{ghe.tenGhe} </span>
                        {(index + 1) % 8 === 0 ? <br /> : ""}
                      </Fragment>
                    );
                  })}
                </span>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="p-3">
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-col text-center w-full mb-20">
            <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-purple-600">
              Lịch sử đặt vé khách hàng
            </h1>
            <p className="lg:w-2/3 mx-auto leading-relaxed text-base font-bold">
              Hãy xem thông tin địa điểm và thời gian để xem phim vui vẻ bạn nhé
            </p>
          </div>
          <div className="flex flex-wrap -m-8 ">{renderTicketItem()}</div>
        </div>
      </section>
    </div>
  );
}
