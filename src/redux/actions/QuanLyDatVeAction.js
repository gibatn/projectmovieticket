import { message } from "antd";
import { quanLyDatVeService } from "../../services/QuanLyDatVeService";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { displayLoadingAction, hideLoadingAction } from "./LoadingAction";
import { DISPLAY_LOADING } from "./types/loadingType";
import {
  CHUYEN_TAB,
  DAT_VE_HOAN_TAT,
  SET_CHI_TIET_PHONG_VE,
} from "./types/QuanLyDatVeType";
export const layChiTietPhongVeAction = (maLichChieu) => {
  return async (dispatch) => {
    try {
      const result = await quanLyDatVeService.layChiTietPhongVe(maLichChieu);

      dispatch({
        type: SET_CHI_TIET_PHONG_VE,
        chiTietPhongVe: result.data.content,
      });
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};
export const datVeAction = (thongTinDatVe = new thongTinDatVe()) => {
  return async (dispatch) => {
    dispatch(displayLoadingAction);

    try {
      let result = await quanLyDatVeService.datVe(thongTinDatVe);
      message.success("Đặt vé thành công");
      await dispatch(layChiTietPhongVeAction(thongTinDatVe.maLichChieu));
      await dispatch({
        type: DAT_VE_HOAN_TAT,
      });
      await dispatch(hideLoadingAction);
      await dispatch({ type: CHUYEN_TAB });
    } catch (error) {
      dispatch(hideLoadingAction);
      console.log(error.response.data);
    }
  };
};
