import { quanLyPhimService } from "../../services/QuanLyPhimService";
import {
  SET_DANH_SACH_PHIM,
  SET_THONG_TIN_FILM,
} from "./types/QuanLyPhimStyle";

export const layDanhSachPhimAction = (tenPhim = "") => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.layDanhSachPhim(tenPhim);
      dispatch({
        type: SET_DANH_SACH_PHIM,
        arrFilm: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const themPhimUploadHinh = (formData) => {
  return async (dispatch) => {
    try {
      let result = await quanLyPhimService.themPhimUpload(formData);
      alert("Thêm thành công");
    } catch (err) {
      console.log(err.response);
    }
  };
};

export const layThongTinPhimAction = (maPhim) => {
  return async (dispatch) => {
    try {
      let result = await quanLyPhimService.layThongTinPhim(maPhim);
      dispatch({
        type: SET_THONG_TIN_FILM,
        filmInfo: result.data.content,
      });
    } catch (error) {
      console.log(error.response);
    }
  };
};

export const capNhatPhimUploadAction = (formData) => {
  return async (dispatch) => {
    try {
      let result = await quanLyPhimService.capNhatPhimUpload(formData);
      alert("Cập nhật phim thành công");
      window.history.go(-1);
    } catch (error) {
      console.log(error.response);
    }
  };
};

export const xoaPhimAction = (maPhim) => {
  return async (dispatch) => {
    try {
      let result = await quanLyPhimService.xoaPhim(maPhim);
      alert("Xóa phim thành công");
      dispatch(layDanhSachPhimAction());
      console.log(result.data.content);
    } catch (error) {
      console.log(error.response);
    }
  };
};
