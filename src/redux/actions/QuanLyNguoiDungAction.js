import { message } from "antd";
import { quanLyNguoiDungService } from "../../services/QuanLyNguoiDung";
import {
  CAP_NHAT_USER_LOGIN,
  DANG_NHAP_ACTION,
  SET_THONG_TIN_NGUOI_DUNG,
} from "./types/QuanLyNguoiDungType";
export const dangNhapAction = (thongTinDangNhap) => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungService.dangNhap(thongTinDangNhap);

      dispatch({
        type: DANG_NHAP_ACTION,
        thongTinDangNhap: result.data.content,
      });
      window.history.go(-1);
    } catch (err) {}
  };
};
export const layThongTinNguoiDungAction = () => {
  return async (dispatch) => {
    try {
      let result = await quanLyNguoiDungService.layThongTinNguoiDung();

      dispatch({
        type: SET_THONG_TIN_NGUOI_DUNG,
        thongTinNguoiDung: result.data.content,
      });
    } catch (error) {}
  };
};

export const capNhatThongTinNguoiDungAction = (data) => {
  return async (dispatch) => {
    try {
      let result = await quanLyNguoiDungService.capNhatThongTinNguoiDung(data);

      message.success("cập nhật thành công");
      dispatch({
        type: CAP_NHAT_USER_LOGIN,
        payload: data,
      });
    } catch (error) {}
  };
};
