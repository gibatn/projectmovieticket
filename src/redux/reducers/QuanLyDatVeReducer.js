import {
  CHANGE_TAB_ACTIVE,
  CHUYEN_TAB,
  CHUYEN_TAB_DEFAULT,
  DAT_VE,
  DAT_VE_HOAN_TAT,
  SET_CHI_TIET_PHONG_VE,
} from "../actions/types/QuanLyDatVeType";
import { ThongTinLichChieu } from "../../_core/models/ThongTinPhongVe";
const stateDefault = {
  chiTietPhongVe: new ThongTinLichChieu(),
  danhSachGheDangDat: [
    // {
    //   daDat: false,
    //   giaVe: 75000,
    //   loaiGhe: "Thuong",
    //   maGhe: 47514,
    //   maRap: 451,
    //   stt: "114",
    //   taiKhoanNguoiDat: null,
    //   tenGhe: "114",
    // },
  ],
  tabActive: "1",
};

export const QuanLyDatVeReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case SET_CHI_TIET_PHONG_VE: {
      return { ...state, chiTietPhongVe: action.chiTietPhongVe };
    }
    case DAT_VE: {
      // update ds ghế đang đặt
      let danhSachGheCapNhap = [...state.danhSachGheDangDat];
      let index = danhSachGheCapNhap.findIndex(
        (gheDD) => gheDD.maGhe === action.gheDuocChon.maGhe
      );
      console.log(index);
      if (index !== -1) {
        danhSachGheCapNhap.splice(index, 1);
      } else {
        danhSachGheCapNhap.push(action.gheDuocChon);
      }
      return { ...state, danhSachGheDangDat: danhSachGheCapNhap };
    }
    case DAT_VE_HOAN_TAT: {
      state.danhSachGheDangDat = [];
      return { ...state };
    }
    case CHUYEN_TAB: {
      state.tabActive = "2";
      return { ...state };
    }
    case CHANGE_TAB_ACTIVE: {
      state.tabActive = action.number;
      return { ...state };
    }
    case CHUYEN_TAB_DEFAULT: {
      state.tabActive = "1";
      return { ...state };
    }
    default:
      return state;
  }
};
