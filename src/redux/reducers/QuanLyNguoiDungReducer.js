import { TOKEN, USER_LOGIN } from "../../util/settings/config";
import {
  CAP_NHAT_USER_LOGIN,
  DANG_NHAP_ACTION,
  DANG_XUAT,
  SET_THONG_TIN_NGUOI_DUNG,
} from "../actions/types/QuanLyNguoiDungType";
let user = {};
if (localStorage.getItem(USER_LOGIN)) {
  user = JSON.parse(localStorage.getItem(USER_LOGIN));
}
const initialState = {
  userLogin: user,
  thongTinNguoiDung: {},
};

export const QuanLyNguoiDungReducer = (state = initialState, action) => {
  switch (action.type) {
    case DANG_NHAP_ACTION: {
      localStorage.setItem(USER_LOGIN, JSON.stringify(action.thongTinDangNhap));
      localStorage.setItem(
        TOKEN,
        JSON.stringify(action.thongTinDangNhap.accessToken)
      );
      return { ...state, userLogin: action.thongTinDangNhap };
    }
    case CAP_NHAT_USER_LOGIN: {
      return { ...state, userLogin: action.payload };
    }
    case DANG_XUAT: {
      localStorage.removeItem(USER_LOGIN);
      localStorage.removeItem(TOKEN);

      return { ...state, userLogin: {} };
    }
    case SET_THONG_TIN_NGUOI_DUNG: {
      state.thongTinNguoiDung = action.thongTinNguoiDung;
      return { ...state };
    }
    default:
      return state;
  }
};
