import {
  SET_DANH_SACH_PHIM,
  SET_FILM_DANG_CHIEU,
  SET_FILM_SAP_CHIEU,
  SET_THONG_TIN_FILM,
} from "../actions/types/QuanLyPhimStyle";
import { SET_CHI__TIET_PHIM } from "../actions/types/QuanLyRapType";

const stateDefault = {
  arrFilm: [],
  dangChieu: false,
  sapChieu: false,
  arrFilmDefault: [],
  filmDetail: {},

  filmInfo: {},
};

export const QuanLyPhimReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case SET_DANH_SACH_PHIM: {
      state.arrFilmDefault = action.arrFilm;
      // state.arrFilm = state.arrFilm.filter(
      //   (film) =>
      //     film.dangChieu === state.dangChieu && film.sapChieu === state.sapChieu
      // );
      return { ...state, arrFilm: action.arrFilm };
    }
    case SET_FILM_DANG_CHIEU: {
      state.dangChieu = !state.dangChieu;
      if (state.dangChieu) {
        state.sapChieu = false;
        state.arrFilm = state.arrFilmDefault.filter((film) => {
          return film.dangChieu === state.dangChieu;
        });
      } else {
        state.arrFilm = state.arrFilmDefault;
      }

      return { ...state };
    }
    case SET_FILM_SAP_CHIEU: {
      state.sapChieu = !state.sapChieu;
      if (state.sapChieu) {
        state.dangChieu = false;
        state.arrFilm = state.arrFilmDefault.filter(
          (film) => film.sapChieu === state.sapChieu
        );
      } else {
        state.arrFilm = state.arrFilmDefault;
      }

      return { ...state };
    }
    case SET_CHI__TIET_PHIM: {
      return { ...state, filmDetail: action.filmDetail };
    }
    case SET_THONG_TIN_FILM: {
      return { ...state, filmInfo: action.filmInfo };
    }
    default:
      return state;
  }
};
