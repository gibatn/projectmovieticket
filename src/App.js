import { createBrowserHistory } from "history";
import "./App.css";
import HomeTemplate from "./templates/HomeTemplate/HomeTemplate";
import Home from "./pages/Home/Home";
import Contact from "./pages/Contact/Contact";
import Login from "./pages/Login/Login";
import Checkout from "./pages/Checkout/Checkout";
import Register from "./pages/Register/Register";
import Profile from "./pages/Profile/Profile";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Detail from "./pages/Detail/Detail";
import { lazy } from "react";
import Loading from "./components/Loading/Loading";
import CheckoutTemplate from "./templates/CheckoutTemplate/CheckoutTemplate";
import AdminTemplate from "./templates/AdminTemplate/AdminTemplate";
import Dashboard from "./pages/Admin/Dashboard/Dashboard";
import Films from "./pages/Admin/Films/Films";
import ShowTime from "./pages/Admin/Showtime/ShowTime";
import AddNew from "./pages/Admin/Films/AddNew/AddNew";
import Edit from "./pages/Admin/Films/Edit/Edit";
const CheckoutTemplateLazy = lazy(() =>
  import("./templates/CheckoutTemplate/CheckoutTemplate")
);

export const history = createBrowserHistory();
function App() {
  return (
    <BrowserRouter>
      <Loading />
      <Routes>
        <Route path="/" element={<HomeTemplate Component={Home} />}></Route>
        <Route
          path="/contact"
          element={<HomeTemplate Component={Contact} />}
        ></Route>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route
          path="/detail/:id"
          element={<HomeTemplate Component={Detail} />}
        />
        <Route path="/register" element={<Register />} />
        <Route path="/profile" element={<HomeTemplate Component={Profile} />} />
        <Route
          path="/checkout/:id"
          element={<CheckoutTemplate Component={Checkout} />}
        />
        {/* <Route
          path="/checkout/:id"
          element={
            <Suspense fallback={<p>huhu</p>}>
              <CheckoutTemplateLazy Component={Checkout} />
            </Suspense>
          }
        /> */}
        {/* ADMIN */}
        <Route
          path="/admin"
          element={<AdminTemplate Component={Dashboard} />}
        />
        <Route
          path="/admin/user"
          element={<AdminTemplate Component={Dashboard} />}
        />
        <Route
          path="/admin/film"
          element={<AdminTemplate Component={Films} />}
        />
        <Route
          path="/admin/film/addnew"
          element={<AdminTemplate Component={AddNew} />}
        />
        <Route
          path="/admin/film/edit/:id"
          element={<AdminTemplate Component={Edit} />}
        />
        <Route
          path="/admin/film/showtime/:id/:tenPhim"
          element={<AdminTemplate Component={ShowTime} />}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
